class freebsd::base {
  # Packages
  package {
    [
      'ssmtp',
      'vim-tiny',
    ]:
      ensure => installed,
  }

  # Make periodic logs go to files instead of mail.
  file { '/etc/periodic.conf':
    ensure => file,
    source => 'puppet:///modules/freebsd/periodic.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  # Redirect syslog to Lysator's server.
  file { '/etc/syslog.conf':
    ensure => file,
    source => 'puppet:///modules/freebsd/syslog.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  # Configure ssmtp
  file { '/usr/local/etc/ssmtp/ssmtp.conf':
    ensure  => file,
    content => template('freebsd/ssmtp.conf.erb'),
    owner   => 'root',
    group   => 'ssmtp',
    mode    => '0640',
  }

  file { '/etc/mail/mailer.conf':
    ensure => file,
    source => 'puppet:///modules/freebsd/mailer.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  # Disable sendmail.
  file_line { 'Disable sendmail':
    path => '/etc/rc.conf',
    line => 'sendmail_enable="NO"',
  }
  file_line { 'Disable sendmail submit':
    path => '/etc/rc.conf',
    line => 'sendmail_submit_enable="NO"',
  }
  file_line { 'Disable sendmail outbound':
    path => '/etc/rc.conf',
    line => 'sendmail_outbound_enable="NO"',
  }
  file_line { 'Disable sendmail msp':
    path => '/etc/rc.conf',
    line => 'sendmail_msp_queue_enable="NO"',
  }
}
