#
class freebsd::workstation::slim {
  file { '/usr/local/etc/slim.conf':
    ensure => file,
    source => 'puppet:///modules/freebsd/workstation/slim.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/usr/local/share/slim-lysator/themes/':
    ensure  => directory,
    recurse => true,
    source  => 'puppet:///modules/freebsd/workstation/slim.themes/',
    owner   => 'root',
    group   => 'wheel',
    mode    => '0644',
  }

  file_line { 'Enable slim login manager':
    path => '/etc/rc.conf',
    line => 'slim_enable="YES"',
  }
}
