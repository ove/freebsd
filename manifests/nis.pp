class freebsd::nis {
  file { '/etc/auto_master':
    ensure => file,
    source => 'puppet:///modules/freebsd/nis/auto_master',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/nsswitch.conf':
    ensure => file,
    source => 'puppet:///modules/freebsd/nis/nsswitch.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/krb5.conf':
    ensure => file,
    source => 'puppet:///modules/freebsd/nis/krb5.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/pam.d/sshd':
    ensure => file,
    source => 'puppet:///modules/freebsd/nis/pam.sshd',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/pam.d/system':
    ensure => file,
    source => 'puppet:///modules/freebsd/nis/pam.system',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/pam.d/su':
    ensure => file,
    source => 'puppet:///modules/freebsd/nis/pam.su',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file_line { 'Set nisdomain':
    path => '/etc/rc.conf',
    line => 'nisdomainname="lysator"',
  }

  file_line { 'Enable nis client':
    path => '/etc/rc.conf',
    line => 'nis_client_enable="YES"',
  }

  file_line { 'Set nis flags':
    path => '/etc/rc.conf',
    line => 'nis_client_flags="-s -m -S lysator,nis.lysator.liu.se,nis-slave.lysator.liu.se"',
  }

  file_line { 'Enable lockd':
    path => '/etc/rc.conf',
    line => 'rpc_lockd_enable="YES"',
  }

  file_line { 'Enable autofs':
    path => '/etc/rc.conf',
    line => 'autofs_enable="YES"',
  }

  file { '/etc/autofs/include_yp':
    ensure => file,
    source => 'puppet:///modules/freebsd/nis/include_yp',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0744',
  }

  file { '/etc/autofs/include':
    ensure => 'link',
    target => '/etc/autofs/include_yp',
  }
}
