#
class freebsd::blacklistd {
  file_line { 'Enable blacklistd':
    path => '/etc/rc.conf',
    line => 'blacklistd_enable="YES"',
  }
  file_line { 'Enable blacklistd support in sshd':
    path => '/etc/rc.conf',
    line => 'sshd_flags="-o UseBlacklist=yes"',
  }
  file { '/etc/ipfw-blacklist.rc':
    ensure  => present,
    content => '',
  }
}
